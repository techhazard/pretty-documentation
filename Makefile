#Author: Vince van Oosten
#Tested on: ubuntu 14.04 64-bit
.PHONY: clean all src 

export

#project name
NAME = pretty-docs

#depends on what I'm working on: js or src
all:  src

#see `all:` in src/
src:
	@$(MAKE) -C src/
	@echo "\nexecutable created\n"


#to clean up all files created by makefile
clean: 
	@$(MAKE) -C src clean
	rm -f html/*.js
	rm -f html/html/toJs

js: html/html/toJs
	@cd html/html/ && \
	find *.* | xargs ./toJs

html/html/toJs: src/toJs.cc
	g++ src/toJs.cc -o html/html/toJs

setup:
	@grep -q $(NAME) .gitignore || echo $(NAME) >> .gitignore

debug:
	gdb ./$(NAME)