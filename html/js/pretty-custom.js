function doDynamicInclude()
{
	$('div').each(function ()
	{
		var dit = this;
		inc = $(dit).data('replace');
		console.log(inc);
		if(inc != undefined)
		{
			$.ajax({
				url: inc,
				dataType: 'html'
			})
			.success(function (data){ $(dit).replaceWith(data);})
			.fail(function (){ alert("something went wrong while loading the page"); });
		}
	});
}

/* on Document Load */
$(function ()
{
	doDynamicInclude();

	$('[data-toggle=offcanvas]').click(function () {
	$('.row-offcanvas').toggleClass('active')
	});
});