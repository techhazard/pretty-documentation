/* Dit is de eerste opdracht voor algoritmiek door 
Vince van Oosten en Karen Goes. (resp. s1437127, s1350331)
Compiler:
gcc version 4.8.1 (Ubuntu/Linaro 4.8.1-10ubuntu9) op 64-bit ubuntu 13.10.
*/

#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>
#include <stdlib.h> 
#include "veld.h"

using namespace std;
// GLOBALE VARIABELEN


Veld* speelbord = new Veld;

void menu()
{
	while(!cin.eof())
	{
		char optie = '0';
		cout << "\n\nkies je optie!"<<endl;
		cout <<"1: optie 1\n2: optie 2\nl: veld laden uit een bestand\nr: bord random vullen\ng: gretig VS random (of andersom)\no: optimaal VS random (of andersom)\n4: gretig VS gretig\na: afsluiten\n"<<endl;
		cout << "optie: ";
		cin >> optie;
		cout << endl<<endl;
		optie = tolower(optie);

		if (optie == 'b')
			speelbord->gretig();

		else if (optie == '1')
			speelbord->eersteOptie();

		else if (optie == '2')
			speelbord->tweedeOptie();

		else if (optie == 'r')
			speelbord->randomBord();

		else if (optie == 'g')
		{
			cout <<"Welke speler speelt gretig?[0/1]:"<<endl;
			cin >> optie;
			if (optie == '1')
				speelbord->gretigRandom(true);
			else
				speelbord->gretigRandom(false);
		}
		else if (optie == 'o')
		{
			cout <<"Welke speler speelt optimaal?[0/1]:"<<endl;
			cin >> optie;
			if (optie == '1')
				speelbord->optimaalRandom(true);
			else
				speelbord->optimaalRandom(false);
		}
		else if (optie == '4')
		{
			speelbord->gretigGretig();
		}


		else if (optie == 'l')
		{
			string naam ="";
			cout << "Voer de naam van het bestand in" << endl;
			cin >> naam;
			if (!speelbord->ophalen(naam))
			{
				cout << "ophalen is niet gelukt..." <<endl;
			}
		}
		else if (optie == 'a')
			return;
		else
		{
			cout << "\n\n\nkies één van de gegeven opties"<< endl;
		}
	}
}


int main(int argc, char const *argv[])
{
	//NAAMCHECK
	if (argc < 2)
	{
		cout <<"\t Geef de bestandsnaam mee! "<< endl <<endl;
		cout <<"\t"<<argv[0]<<" BESTANDSNAAM"<< endl;
	}
	else
		cout << "\t\t\t\t\t\t\tNAAM MEEGEGEVEN" <<endl;

	if (!speelbord->ophalen(argv[1]))
	{
		return 2;
	}
	menu();
	cout << "\t\t\t\t\t\t\tEINDE PROGRAMMA"<<endl;
	return 0;
}
