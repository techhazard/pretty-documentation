#include "zetten.h"

class Veld
{
public:
	Veld();
	~Veld();
	bool ophalen(std::string);
	void mensZet();
	void gretig();
	void gretigRandom(bool);
	void optimaalRandom(bool);
	void eersteOptie();
	void tweedeOptie();
	void randomBord();
	void gretigGretig();
	bool speler;				//speler0 (F) vs speler1 (T);
	int score0, X0, Y0;
	int score1, X1, Y1;
	int Mmax;					//rijen		Y	
	int Nmax;					//kolommen	X
	int laatsteX, laatsteY;		//laatste zet
	int hX, hY;					//X en Y van hoogste waarde
	int speelVeld[50][50];
	Stapel* stapel;
private:
	void afdrukken();
	bool checkScore(int&);
	void geenZet();
	bool doeZet(int,int);
	bool gretigeZet();
	int hoogste(int&, int&);
	void maakLeeg();
	bool optieEen();
	bool optieTwee(int&, int&, int&);
	bool zetTerug();
	void maakpermutatie(int A[], int n);
	bool winst();
	bool optimaleZet(int&, int&);
	void randomZet();
	bool emptyCheck(bool);
};