#include <fstream>
#include <iostream>
#include <cstdlib>
#include <string>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */
#include "veld.h"

using namespace std;


/* PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC PUBLIC */

Veld::Veld()
{
	stapel = new Stapel;
	score0 = 0;
	score1 = 0;
	speler = false;
}

Veld::~Veld()
{
	maakLeeg();
}


bool Veld::ophalen(string naam)
{

fstream invoer(naam.c_str(), ios::in); // !!!
if (invoer.fail()) 
{
	cout << '"'<< naam << "\" niet te openen" << endl;
	return 2;
}//if
invoer >> Mmax;
invoer >> Nmax;
if (max(Mmax,Nmax)>50)
{
	return false;
}
maakLeeg();

//veld vullen
for (int i = 0; i < Mmax; ++i)
{
	for (int j = 0; j < Nmax; ++j)
	{
		invoer >> speelVeld[i][j];
		if (speelVeld[i][j]>=101 || (speelVeld[i][j]< -1))
		{
			cout <<"er is een ongeldig getal gevonden.\n gelieve groter dan -2 en kleiner dan 100"<<endl;
			return false;
		}
		if (speelVeld[i][j] ==-1)
		{
			stapel->push(0, j, i);
			laatsteX = j;
			laatsteY = i;
		}
	}
}
cout <<"\t\t\t\t\t\t\tSPEELVELD GEVULD" <<endl;
invoer.close();
score0 = 0;
score1 = 0;
speler = false;
afdrukken();

return true;
}

void Veld::mensZet()
{
	int x, y;
	cout << "voer een breedtecoördinaat in tussen 0 en "<< Nmax <<":" << endl;
	cin >> x;
	cout << "voer een hoogtecoördinaat in tussen 0 en "<< Mmax <<":" << endl;
	cin >> y;
	doeZet(x,y);
}


void Veld::gretig()
{
	gretigeZet();
}

void Veld::gretigRandom(bool omkeren)
{
	int x = -1;
	int y = -1;
	if (!omkeren)
	{
		while (hoogste(x,y))
		{
			gretigeZet();
			randomZet();
		}
	}
	else
	{
		while (hoogste(x,y))
		{
			randomZet();
			gretigeZet();
		}
	}
	int s;
	speler = checkScore(s);
	cout <<"speler "<<speler<<" wint met ";
	if (speler)
		cout << score1;
	else 
		cout <<score0;
	cout <<" punten."<<endl;

	return;
}

void Veld::optimaalRandom(bool omkeren)
{
	int x = -1;
	int y = -1;
	if (!omkeren)
	{
		while (hoogste(x,y))
		{
			optimaleZet(x,y);
			randomZet();
		}
	}
	else
	{
		while (hoogste(x,y))
		{
			randomZet();
			optimaleZet(x,y);
		}	
	}
	int s;
	speler = checkScore(s);
	afdrukken();
	cout <<"speler "<<speler<<" wint met ";
	if (speler)
		cout << score1;
	else 
		cout <<score0;
	cout <<" punten."<<endl;

	return;
}

void Veld::gretigGretig()
{
	int x = -1;
	int y = -1;
	while (hoogste(x,y))
	{
		gretigeZet();
		afdrukken();
	}

	int s;
	speler = checkScore(s);
	cout <<"speler "<<speler<<" wint met ";
	if (speler)
		cout << score1;
	else 
		cout <<score0;
	cout <<" punten."<<endl;

	return;
}


void Veld::eersteOptie()
{
	clock_t t = clock();
	cout <<"\nspeler "<<optieEen()<<" wint"<<endl;
	cout << "met deze beginstand: " <<endl;
	t = clock() - t;
	double tijd = ((double)t)/CLOCKS_PER_SEC;
	cout <<"("<< tijd <<"s)"<<endl;
	afdrukken();		
}

void Veld::tweedeOptie()
{
	clock_t t = clock();

	int sp1 = +10000;
	int sp0 = -10000;
	int verschil = 0;
	bool spelerWin = false;
	int scoreWin = 0;
	spelerWin = optieTwee(sp0, sp1, verschil);
	if (verschil >0)
	{
		scoreWin = verschil;
	}
	else
	{
		spelerWin = true;
		scoreWin = abs(verschil);
	}
	cout <<"speler "<< spelerWin <<" wint bij een perfect spel\n"; 
	cout <<"met een verschil van "<< scoreWin <<" punten" <<endl;
	cout << "sp0:"<< sp0 <<" sp1:"<< sp1 <<" ver:"<< verschil <<endl;
	t = clock() - t;
	double tijd = ((double)t)/CLOCKS_PER_SEC;
	cout << tijd <<"s ("<<t<<" CLOCKS)"<<endl;
	afdrukken();
}


// stop random permutatie van 0,1,...,n-1 in array A
void Veld::maakpermutatie (int A[ ], int n)
{
	int i; // array-index
	int r; // random array-index
	int temp; // random array-index
	for (i = 0; i < n; i++) 
		A[i] = i;

	for (i = n-1; i >= 0; i--)
	{
		r = rand()%(i+1); // 0 <= r <= i
		temp = A[i];
		A[i] = A[r];
		A[r] = temp;
	}//for
}//maakpermutatie


// hetzelfde random algoritme as in de slides van pm.
void Veld::randomBord()
{

	int x,y;
	int r = 0;
	cout << "Dit genereert een redelijk willekeurig M*N speelbord."<<endl;
	cout << "voer een M in:";
	cin >> Mmax;
	cout << "voer een N in:";
	cin >> Nmax;
	if (max(Mmax,Nmax)>50)
	{
		cout << "niet groter dan 50!"<<endl;
		return;
	}

	int A[Mmax*Nmax];

	maakpermutatie(A, Mmax*Nmax);


	for (x = 0; x < Nmax; x++)
	{
		for (y = 0; y < Mmax; y++)
		{
			if(A[r] == 0)
			{
				A[r] = -1;
				laatsteX = x;
				laatsteY = y;
			}
			speelVeld[y][x] = A[r];
			r++;
		}//for
	}
	score0 = 0;
	score1 = 0;
	speler = false;
	afdrukken();
}//maakpermutatie










/* PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE PRIVATE */

void Veld::afdrukken()
{
	cout << endl;
	for (int i = 0; i < Mmax; ++i)
	{
		for (int j = 0; j < Nmax; ++j)
		{
			switch (speelVeld[i][j])
			{
				case -1: 
				cout <<" * ";
				break;
				case 0: 
				cout <<" - ";
				break;
				default: 
				if (speelVeld[i][j]>9)
					(cout << speelVeld[i][j] << " ");
				else //om het veld mooi gelijk te krjgen
					(cout << " "<<speelVeld[i][j] << " ");
				break;
			}
		}
		cout << endl;
	}
	cout <<"0:"<< score0 <<" 1:"<< score1<<endl;
	cout << "\t\t\t\t\t\t\tSPEELVELD AFGEDRUKT"<<endl;
}

bool Veld::checkScore(int& verschil)
{
	verschil = score0 - score1;
//als speler 0 voorstaat, return 	
	if (verschil > 0)
		return false;
//bij gelijk spel wint 1
	return true;
}


//doet een zet 
bool Veld::doeZet(int x, int y)
{
	if (min(x,y) < 0 || y >= Mmax || x >= Nmax)
	{
		geenZet();
		return false;
	}

//controleert of dit de eerste zet is
	if (stapel->laatste_zet == NULL)  
	{
		speelVeld[laatsteY][laatsteX] = 0;
	}

	//info in nieuwe zet opslaan 
	stapel->push(speelVeld[y][x], x, y);

//score bijwerken
	if (speler)
		score1 += speelVeld[y][x];
	else
		score0 += speelVeld[y][x];

//spelerwissel
	speler = !speler;

//speciale vakje verplaatsen
	speelVeld[laatsteY][laatsteX] = 0;
	laatsteX = x;
	laatsteY = y;
	speelVeld[y][x] = -1;
//	afdrukken();
	return true;
}

//als er geen zet meer mogelijk is
void Veld::geenZet()
{
	cout << "Dit is geen geldige zet." << endl;
	afdrukken();
}

//pakt de hoogste
bool Veld::gretigeZet()
{
	int x, y;
	hoogste(x,y);
	return doeZet(x,y);
}

//geeft via x en y de locatie van de hoogste waarde
int Veld::hoogste(int& x, int& y)
{
	int hoog = 0;
	x = -1;
	y = -1;
	for (int i = 0; i < Mmax; i++)
	{
		if (speelVeld[i][laatsteX] > hoog)
		{
			hoog = speelVeld[i][laatsteX];
			y = i;
			x = laatsteX;
		}
	}
	for (int j = 0; j < Nmax; j++)
	{
		if (speelVeld[laatsteY][j] > hoog)
		{
			hoog = speelVeld[laatsteY][j];
			y = laatsteY;
			x = j;
		}
	}
	return hoog;
}


void Veld::maakLeeg()
{
	for (int i = 0; i < 50; ++i)
	{
		for (int j = 0; j < 50; ++j)
		{
			speelVeld[i][j] = 0;
		}
	}
	cout << "\t\t\t\t\t\t\tSPEELVELD LEEG" <<endl;
	score0 = 0;
	score1 = 0;
	speler = false;
	delete stapel;
	stapel = new Stapel;
	cout << "\t\t\t\t\t\t\tSCORES RESET" <<endl;
}


//zou recursief alle zetten langs moeten gaan

bool Veld::optieEen()
{
//'speler' is aan de beurt
//is de zet mogelijk?
	bool zetbekeken = false;
	for (int i = 0; i < Mmax; ++i)
	{
		if (speelVeld[i][laatsteX] > 0)
		{
			zetbekeken = true;
			doeZet(laatsteX,i);
//spelerwissel, dus !speler is aan de beurt

			if (optieEen() == !speler) //als deze speler wint
			{
				zetTerug();			//spelerwissel
//deze spelerwissel, dus !!speler is aan de beurt
				return speler;
			}
			zetTerug();
//of deze spelerwissel, dus !!speler is aan de beurt
		}
	}
	//'speler' is aan de beurt
	for (int j = 0; j < Nmax; ++j)
	{
		if (speelVeld[laatsteY][j] > 0)
		{
			zetbekeken = true;
			doeZet(j,laatsteY);	
//spelerwissel, dus !speler is aan de beurt
			if (optieEen() == !speler)
			{
				zetTerug();
//deze spelerwissel, dus !!speler is aan de beurt
				return speler;
			}
			zetTerug();
//of deze spelerwissel, dus !!speler is aan de beurt
		}
	}
	
	if (zetbekeken) //en geen winst gevonden
	{
		return !speler; //omdat de andere speler wint
	}
	int s;
	return checkScore(s);
}

bool Veld::optieTwee(int& sp0, int& sp1, int& verschil)
{										//SPELER
	bool zetbekeken = false;
	for (int i = 0; i < Mmax; ++i)
	{
		if (speelVeld[i][laatsteX] > 0)
		{
			zetbekeken = true;
			doeZet(laatsteX,i);			//!SPELER

			if (optieTwee(sp0, sp1, verschil) == speler) //als deze speler niet wint
			{
				zetTerug();				//SPELER
				return !speler;
			}
			zetTerug();					//SPELER
		}
	}

	for (int j = 0; j < Nmax; ++j)
	{
		if (speelVeld[laatsteY][j] > 0)
		{
			zetbekeken = true;
			doeZet(j,laatsteY);			//!SPELR
			if (optieTwee(sp0, sp1, verschil) == speler)
			{
				zetTerug();				//SPELER
				return !speler;
			}
			zetTerug();					//SPELER
		}
	}
	if (!zetbekeken)
	{
		int s;
		bool winnaar = checkScore(s);
		cout <<"s0:"<< score0 <<" s1:"<<score1<<endl;

		if (speler)
		{	
			if (s < verschil)
			{	
				sp1 = score1;
				verschil = s;
				sp0 = score0;
			}
		}
		else
		{
			if (s > verschil)
			{	
				sp1 = score1;
				verschil = s;
				sp0 = score0;
			}
		}
		return winnaar;
	}
	return speler;
}

//geeft terug of er een winnnaar is.
bool Veld::winst()
{
	int x,y;

	if (!hoogste(x,y)) //dan is het veld leeg
	{
		//dan heeft een speler gewonnen
		//winnaar = checkScore(s);
		return true;
	}
//als nog niemand gewonnen heeft, return false
	return false;
}

//maakt een zet ongedaan
bool Veld::zetTerug()
{
//zet ongedaan maken
	if (stapel->laatste_zet == NULL) //als de stapel leeg is
		return true;
	int x = stapel->laatste_zet->zX;
	int y = stapel->laatste_zet->zY;
	speelVeld[y][x] = stapel->laatste_zet->score;

//spelerwissel
	speler = !speler;

//score bijwerken
	//cout << score0<<", "<<score1<<endl;
	if (speler)
		score1 -= speelVeld[y][x];
	else
		score0 -= speelVeld[y][x];
	//cout << score0<<", "<<score1<<endl;

//speciale vakje verplaatsen
	stapel->pop(x,y);

	laatsteX = x;
	laatsteY = y;
	speelVeld[y][x] = -1;
	//afdrukken();
	return true;
}

bool Veld::optimaleZet(int& x, int& y)
{
//'speler' is aan de beurt
//is de zet mogelijk?
	bool zetbekeken = false;
	for (int i = 0; i < Mmax; ++i)
	{
		if (speelVeld[i][laatsteX] > 0)
		{
			zetbekeken = true;
			doeZet(laatsteX,i);
//spelerwissel, dus !speler is aan de beurt

			if (optimaleZet(x,y) == !speler) //als deze speler wint
			{
				x = stapel->laatste_zet->zX;
				y = stapel->laatste_zet->zY;
				zetTerug();			//spelerwissel
//deze spelerwissel, dus !!speler is aan de beurt
				return speler;
			}
			zetTerug();
//of deze spelerwissel, dus !!speler is aan de beurt
		}
	}


	//'speler' is aan de beurt
	for (int j = 0; j < Nmax; ++j)
	{
		if (speelVeld[laatsteY][j] > 0)
		{
			zetbekeken = true;
			doeZet(j,laatsteY);	
//spelerwissel, dus !speler is aan de beurt
			if (optimaleZet(x,y) == !speler)
			{
				x = stapel->laatste_zet->zX;
				y = stapel->laatste_zet->zY;
				zetTerug();
//deze spelerwissel, dus !!speler is aan de beurt
				return speler;
			}
			zetTerug();
//of deze spelerwissel, dus !!speler is aan de beurt
		}
	}
	
	if (zetbekeken) //en geen winst gevonden
	{
		return !speler; //omdat de andere speler wint
	}
	
	int s;
	return checkScore(s);
} // optimaleZet


void Veld::randomZet()
{
	int A[Mmax];
	int B[Nmax];
	int i,j; // array-index
	int r; // random array-index
	int temp; // random array-index
	for (i = 0; i < Mmax; i++) 
	{
		A[i] = i;
	}
	for (j = 0; j < Nmax; j++) 
	{
		B[j] = j;
	}

	for (i = Mmax-1; i >= 0; i--)
	{
		r = rand()%(i+1); // 0 <= r <= i
		temp = A[i];
		A[i] = A[r];
		A[r] = temp;
	}//for
	for (j = Nmax-1; j >= 0; j--)
	{
		r = rand()%(j+1); // 0 <= r <= j
		temp = B[j];
		B[j] = B[r];
		B[r] = temp;
	}//for

	int x = B[0];
	j = 0;
	bool xy = rand()%(2);
	if(xy && !emptyCheck(false))
	{
		while (((speelVeld[laatsteY][x]<1) && (j<Nmax) ) || (x==laatsteX))
		{
			j++;
			x = B[j];	
		}
		doeZet(x,laatsteY);
	}
	else if (!emptyCheck(true))
	{
		int y = A[0];
		int i = 0;
		while ((speelVeld[y][laatsteX]<1 && i<Mmax )|| (y==laatsteY))
		{
			y = A[i];
			i++;
		}
		doeZet(laatsteX,y);
	}
} //randomZet


bool Veld::emptyCheck(bool xy)
{
	if (xy) //checkt verticaal
	{
		for (int i = 0; i < Mmax; ++i)
		{
			if (speelVeld[i][laatsteX]>0)
			{
				return false;
			}
		}
	}
	else //checkt horizontaal
	{
		for (int j = 0; j < Nmax; ++j)
		{
			if (speelVeld[laatsteY][j]>0)
			{
				return false;
			}
		}
	}
	return true;
}