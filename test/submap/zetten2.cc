#include "zetten.h"
#include <string>

using namespace std;
Zet::Zet()
{
	vorige_zet = NULL;
	score = 0;
	zX = -1;    //omdat (0,0) bestaat
	zY = -1;
}

Zet::~Zet()
{

}

Stapel::Stapel()
{
	laatste_zet = NULL;
}
Stapel::~Stapel()
{
	Zet* temp;
	if (laatste_zet != NULL)
	{
		while (laatste_zet->vorige_zet != NULL)
		{
			temp = laatste_zet->vorige_zet;
			laatste_zet = laatste_zet->vorige_zet->vorige_zet;
			delete temp;
		}
	}
	delete laatste_zet;
}

//maakt nieuwe zet aan
void Stapel::push(int punten, int x, int y)
{
	Zet* temp = laatste_zet;
	laatste_zet = new Zet;
	laatste_zet->vorige_zet = temp;
	laatste_zet->score = punten;
	laatste_zet->zX = x;
	laatste_zet->zY = y;
}

void Stapel::pop(int& x, int& y)
{
	Zet* temp = laatste_zet;
	laatste_zet = laatste_zet->vorige_zet;
	if (temp != NULL)
		delete temp;
	
	if (laatste_zet != NULL)
	{
		x = laatste_zet->zX;
		y = laatste_zet->zY;
	}
}

