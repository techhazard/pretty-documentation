/** \file
contains all the methods to parse a single sourcefile, header.
*/
#ifndef __File_H_INCLUDED__
#define __File_H_INCLUDED__
#include "html.h"
#include "class_instance.h" /*class_instance*/

class File
{
public:
	File(std::string);
	~File();
private:
	int indentation; //level of {'s
	std::string location;
	std::vector<class_instance> fileClasses;
	std::fstream inFile;
	std::string currChars;
	bool inClass, inFunction, inStruct;
	bool commentBlock;
	std::string description;
	std::string blurb;


	void getClass(std::string);
	void parseFile();
	void checkForBraces	(std::string);
	std::string takeLine();
	void processFile();
	std::string getOpeningBrace();
	bool processHeader();
};






#endif