/** #F
Defines the members of the Folder class.
*/
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <dirent.h>

#include "globals.h" /*<string> & <vector>*/
#include "folder.h"
using namespace std; 


vector<string> srcFiles;



/*#
constructor for #Folder class.
lists all the files and then call the Files class for each file.
*/
Folder::Folder()
{
	putOut("Folder",3);
}

Folder::~Folder()
{
	putOut("~Folder",3);
}


/*#
recursively lists all the files in the given directory.
and puts them into the global srcFiles vector
(does not list directories)
(this might list dirs without read access as a file)

#P path the path to recurse into.
#R true if it succeeded in opening a directory, false otherwise.
*/

bool Folder::listContent(string path)
{
	putOut("listContent",3);
/*
for every item in path
	
	if not recurse listContents(path+name) 
		srcFiles.add(path + name)
*/

	if (path == relpath)
		srcFiles.clear();
	struct dirent *drnt;
	DIR *dr;

	if (!(dr=opendir(path.c_str())))
	{
		putOut("file found",2);
		return false;
	}
	drnt=readdir(dr);
	while (drnt != NULL)
	{	
		if (drnt->d_name[0] != '.')
		{
			if (! (listContent(path+drnt->d_name+'/')))
				srcFiles.push_back(path+drnt->d_name);
		}
		drnt=readdir(dr);
	}
	closedir(dr);
	return true;
}




/*#
uses File class to process files
*/
bool Folder::processFiles()
{
	putOut("processFiles",3);
	unsigned int i;
	for (i = 0; i < srcFiles.size(); ++i)
	{
		putOut("processing "+srcFiles[i],3);
		if (! sourceFile.processFile(srcFiles[i]))
			return 0;
	}
	return 1;
}

void Folder::makeFolder(string path)
{
	putOut("makeFolder",3);
	path = relpath + path;
	string command = "mkdir -p "+path;
	system(command.c_str());
}