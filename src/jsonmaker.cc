/*#F
	contains dependencies and definitions for the Json class
*/

#include <fstream>
#include <iostream>
#include <cstdlib>

#include "globals.h"
#include "jsonmaker.h"
using namespace std;


/*#
	initializes all the common defaults.
	because of multiple constructors.
	will need to switch this around (one constructor, runs funcs)
	#P name the name of the file (for the hash)
	#R nothing
*/
JsonMaker::JsonMaker()
{
	putOut("JsonMaker",3);
	//extra large buffer to avoid stack smashing  (^^,)
	outFile.rdbuf()->pubsetbuf(mybuffer,4096);

	//init stuff
	level = 0;
}


/*#
	flushes the stream and closes the file.
*/
JsonMaker::~JsonMaker()
{
	putOut("~JsonMaker",3);
	if (outFile.is_open())
	{
		outFile.flush();
		outFile.close();
	}
}

bool JsonMaker::openOfstream(string name)
{
	putOut("openOfstream",3);
	size_t pos = (name.rfind("/"));
	if (pos != string::npos)
		fileName = name.substr(pos+1,name.length()-1);

	//opening the file to write to
	string newFile = relpath +"../docs/" + makeHash(name)+"."+fileName+".json";
	outFile.open(newFile.c_str(),fstream::out);
	if (!(outFile.is_open()))
	{
		putOut("Can't open file "+newFile, 0);
		return 0; //can't open stream
	}
	return 1; //opened stream
}

/*#
	writes the details of a file to disk.
	#P file the file_instance to be printed.
	#R true if the write is succesful
*/
bool JsonMaker::writeFile(file_instance file)
{
	putOut("writeFile",3);
	if (! openOfstream(file.name))
		return 0; //can't open for writing

	putChar('{');
	putString("name", file.name);
	putString("id", makeHash(file.name));
	putString("blurb",file.blurb);
	putString("description",file.description);
	putStringVector("includes",file.included);
	putContentVector("contents",file.contents);
	putChar('}');
	
	return 1; //succesfully written
}



/*#
	writes the details of a class to disk.
	#P file the class_instance to be printed.
*/
bool JsonMaker::writeClass(class_instance file)
{
	putOut("writeClass",3);
	if (! openOfstream(file.name))
		return 0;

	putChar('{');
	putString("name", file.name);
	putString("id", makeHash(file.name));
	putString("blurb",file.blurb);
	putString("description",file.description);
	putFunctionVector("functions",file.function); //vector<mem_function>
	putVariableVector("variables",file.variable); //vector<mem_variable>
	putChar('}');

	outFile.close();
	return 1; //succesfully written
}

/*#
	makes an 'a-z' hash of the default integer hash.
	which is prettier  :-P.
	probably not cryptographically secure, but wee don't need that here.
	#P str the regular string to be hashed
	#R string hash (a-z)
*/
string JsonMaker::makeHash(string str)
{
	putOut("makeHash",3);
	string strHash = "";
	size_t hashed = hash_str(str);
	while (hashed > 0)
	{
		strHash += (char) (97 + (hashed % 26));
		hashed = hashed / 26;
	}
	return strHash;
}


/*#
	inserts key-value pair into the json file
	#P key to write
	#P value to write
	#R void
*/
void JsonMaker::putString(string key, string value)
{
	putOut("putString",3);
	indentLine();
	outFile << "\""<<key<<"\": \""<<value<<"\",";
}


/*#
	inserts array of values into the json file
	#P key
	#P value[] array of the values
	#R void
*/
void JsonMaker::putStringVector(string key, vector<string>& value)
{
	putOut("putStringVector",3);
	indentLine();
	outFile << "\""<<key<<"\": [ ";

	for (unsigned int v = 0; v<value.size(); v++)
	{
		indentLine();
		outFile <<"\t\""<< value[v] <<"\",";
	}
	removeLast();
	indentLine();
	outFile << "],";
}

/*#
	inserts '{' or '}'.
	and indentation if needed
	#P k the curly brace to write
*/
void JsonMaker::putChar(char k)
{
	putOut("putChar",3);
	switch (k)
	{
	case '{':
		indentLine(0);
		if (level)
			outFile<<"  ";
		outFile <<k;
		level++;
		break;
	case'}':
		removeLast();
		level--;
		indentLine();
		if (level)
			outFile<<"  "<<k<<',';
		else
			outFile <<k<<endl;
		break;
	}

}

/*#
	starts a new line with indentation.
	with an optional offset of tabs
	#P offset amount of tabs to offset; positive will indentLine more.
*/
void JsonMaker::indentLine(int offset)
{
	putOut("indentLine-offset",3);
	if (level > 0)
		outFile <<"\n";
	for (int i = 0; i<(level + offset); i++)
		outFile << "\t";
}
/*
	offset-less version of above
*/
void JsonMaker::indentLine()
{
	putOut("indentLine",3);
	outFile <<"\n";
	for (int i = 0; i<level; i++)
		outFile << "\t";
}


/*#
	used to remove last output char from file.
	primarily used for trailing commas, hence the old name (noComma).
	#R void
*/
void JsonMaker::removeLast()
{
	putOut("removeLast",3);
	long pos = outFile.tellp();
	outFile.seekp (pos-1);
	return;
}

/*#
	inserts key with contents's.
	writes an array with 0 or more content's as json-objects
	#P key
	#P includes the vector containing content's
*/
void JsonMaker::putContentVector(string key, vector<content>& includes)
{
	putOut("putContentVector",3);
	indentLine();
	if (includes.size() == 0)
		return;
	outFile << "\""<<key<<"\": [ ";
	for (unsigned int i = 0; i < includes.size(); ++i)
	{
		putChar('{');
		putString("name",includes[i].name);
		putString("id",makeHash(includes[i].name));
		putString("type",includes[i].type);
		putChar('}');
	}
	removeLast();
	indentLine();
	outFile << "],";
}


/*#
	inserts key with 0 or more member functions.
	writes an array with 0 or more mem_function's as json-objects
	#P key
	#P function the vector containing mem_function's
*/
void JsonMaker::putFunctionVector(string key, vector<mem_function>& function)
{
	putOut("putFunctionVector",3);
	if (function.size() == 0)
		return;
	indentLine();
	outFile << "\""<<key<<"\": [ ";
	for (unsigned int i = 0; i < function.size(); ++i)
	{
		putChar('{');
		putString("name",function[i].name);
		putString("id",makeHash(function[i].name));
		putString("blurb",function[i].blurb);
		putString("description",function[i].description);
		putParameterVector("parameters", function[i].parameter);
		putChar('}');
	}
	removeLast();
	indentLine();
	outFile << "],";
}

/*#
	inserts key with 1 or more function parameters.
	writes an array with 0 or more func_param's as json-objects
	#P key
	#P parameter the vector containing func_param's
*/
void JsonMaker::putParameterVector(string key, vector<func_param>& parameter)
{
	putOut("putParameterVector",3);
	indentLine();
	outFile << "\""<<key<<"\": [ ";
	for (unsigned int i = 0; i < parameter.size(); ++i)
	{
		putChar('{');
		putInt("p",parameter[i].p);
		putString("name",parameter[i].name);
		putString("type",parameter[i].type);
		putString("blurb",parameter[i].blurb);
		putChar('}');
	}
	indentLine();
	outFile << "],";
}


/*#
	inserts key with 0 or more member variables.
	writes an array with 0 or more mem_varable's as json-objects
	#P key
	#P variable the vector containing mem_variable's
*/
void JsonMaker::putVariableVector(string key, vector<mem_variable>& variable)
{
	putOut("putVariableVector",3);
	if (variable.size() == 0)
		return;
	indentLine();
	outFile << "\""<<key<<"\": [ ";
	for (unsigned int i = 0; i < variable.size(); ++i)
	{
		putChar('{');
		putString("name",variable[i].name);
		putString("id",makeHash(variable[i].name));
		putString("blurb",variable[i].blurb);
		putString("description",variable[i].description);
		putChar('}');
	}
	indentLine();
	outFile << "],";
}

void JsonMaker::putInt(string key, int value)
{
	putOut("putInt",3);
	indentLine();
	//perhaps with an ascii shift or something
	outFile << "\""<<key<<"\": \""<<value<<"\",";
}