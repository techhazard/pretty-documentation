/** \file
contains all the methods to parse a single directory of sourcefiles, header.
*/
#ifndef __Folder_H_INCLUDED__
#define __Folder_H_INCLUDED__

#include "fileprocessor.h"


class Folder
{
public:
	Folder();
	~Folder();
	bool listContent(string);
	bool processFiles();
	void makeFolder(string);
private:
	FileProcessor sourceFile;
};





#endif