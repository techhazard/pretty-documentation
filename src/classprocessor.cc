/*#F
Contains definitions for the class: 'Class'.
*/
#include <fstream>
#include "globals.h"

#include "classprocessor.h"

using namespace std;
/*#
	parses a class.
	and puts it in a varible of type class_instance
	#P line the string containing the 'class keyword'
	#P pos the starting position of this keyword
	#R nothing
*/
ClassProcessor::ClassProcessor()
{
	putOut("ClassProcessor",3);
	indentation = 0;
	className = ""; /* = scope::*/;
	// delete writer;
	// delete classInstance;
	// classInstance = new class_instance;
	// writer = new JsonMaker;
}


/*#
	writes class to json.
	by passing it to a json instance.
*/
ClassProcessor::~ClassProcessor()
{
	putOut("~ClassProcessor",3);
	putOut("writing class json",2);
	writer.writeClass(classInstance);
	putOut("done",2);
}




bool ClassProcessor::processClass(string& line, size_t pos)
{
	putOut("processClass",3);
	// dummyClass();
	classInstance.name = "";
	size_t end = line.find(" ", pos+6) -1;
	if (end == string::npos)
		end = line.find("{", pos+6) -1;
	if (end == string::npos)
		end = line.find("\n", pos+6) -1;
	classInstance.name += line.substr(pos+6,end);

	checkStr(line);
	do {
		line = FileProcessor::takeLine();
		checkStr(line);

	} while (indentation > 0);

	return 1;
}


/*#
	this parses a string that ends with a ';'.
	into a function or variable;
	#P str the string to parse;

*/
void ClassProcessor::parseSubItem(string str)
{
	putOut("parseSubItem",3);
	size_t pos = str.find("(");
	size_t beg = str.rfind(" ");
	if (beg == string::npos)
		beg = 0;
	if (pos == string::npos)
	{
		putOut("mem_var",3);
		mem_variable variable;
		variable.name = str.substr(beg,str.length()-2);
		variable.type = str.substr(0,beg);
		classInstance.variable.push_back(variable);
	}
	else
	{
		putOut("mem_func",3);
		mem_function function;
		//between '(' and first ' ' before it;
		function.name = str.substr(beg,pos-1);
		func_param parameter;
		function.parameter = (getParameters(str));
	}
	
}



/*#
	parses parameters of a function.
	and returns them in a vector containing func_param instances.
	#R vector of parameters
	#P function a string containing the function
*/
vector<func_param> ClassProcessor::getParameters(string function)
{
	putOut("getParameters",3);
	vector<func_param> parameters;
	func_param parameter;
	parameter.p = 0;
	parameter.type = "void";
	size_t beg = function.find("(");
	size_t mid;
	size_t end = function.rfind(" ",beg); //'reverse end' with rfind

	if (end != string::npos)
		parameter.type = function.substr(0,end -1);
	parameter.blurb = "dummy";
	//return value
	parameters.push_back(parameter);

	do
	{
		parameter.p++;
		end = function.find(",",beg);

		//no "," found
		if (end == string::npos)
		{
			//check for ")" last/only parameter
			end = function.find(")",beg);
			
			// ) is after last parameter or directly after (,
			//so no parameter to process
			if (end == beg+1)
				break;
		}
		//find separation between type and name
		mid = function.rfind(" ",end);
		if (mid == string::npos)
		{
			parameter.type = function.substr(beg,end);
			parameter.name = "";
		}
		else
		{
			parameter.type = function.substr(beg,mid);
			parameter.name = function.substr(mid,end);
		}
		putOut("p_type:"+parameter.type +" p_name:"+parameter.name,3);
	}
	while(1);

	return parameters;
}



/*#
	checks string for curly braces or ';'.
	will send to parseSubItem if ';' is found
	#P line the string to be checked
	#R void
*/
void ClassProcessor::checkStr(string line)
{
	putOut("checkStr",3);
	for (unsigned int i = 0; i < line.length(); ++i)
		{
			switch (line[i])
			{
				case '{':
					indentation++;
					break;

				case '}':
					indentation--;
					break;

				case ';':
					parseSubItem(line.substr(0,i));
					break;
			}

		}
}

//temporary until class is read correctly
void ClassProcessor::dummyClass()
{
	putOut("dummyClass",3);
	classInstance.blurb = "wqerwggf";
	classInstance.description = "wewggferqEWFDWQRWGFDG";

	mem_variable vari;
		vari.name = "var1";
		vari.id = "hash";
		vari.blurb = "fffdsfd";
		vari.description = "defeqwffbefwgfbefg";
		vari.type = "mem_variable";
		vari.hidden = 1;
	classInstance.variable.push_back(vari);
	putOut("push_var",-1);
		vari.name = "var2";
		vari.id = "hash";
		vari.blurb = "fffdsfd";
		vari.description = "defeqwffbefwgfbefg";
		vari.type = "int";
		vari.hidden = 1;
	classInstance.variable.push_back(vari);
	putOut("push_var",-1);

	mem_function func;
		func.name = "func1";
		func.blurb = "effdbg";
		func.description = "efgdgbfewgregewr";
		func_param params;
		putOut("mem_func",-1);
			params.p = 0;
			params.type = "void";
			params.name = "name";
			params.blurb = "nothing";
		func.parameter.push_back(params);
		putOut("push_parm",-1);
		func_param params2;
			params2.p = 1;
			params2.type = "int";
			params2.name = "parm";
			params2.blurb = "integerdfddfd";
		func.parameter.push_back(params2);
		putOut("push_parm",-1);

	classInstance.function.push_back(func);
	putOut("push_func",-1);
	classInstance.function.push_back(func);
	putOut("push_func",-1);
}
