#ifndef __CLASS_CLASS_H_INCLUDED__
#define __CLASS_CLASS_H_INCLUDED__
/*#F
	class processor header blah blah
*/

#include "class_instance.h"
#include "jsonmaker.h"
#include "fileprocessor.h"

class ClassProcessor : FileProcessor
{
	int indentation; //level of {'s
	class_instance classInstance;
	JsonMaker writer;
	string className;
	FileProcessor fileProcessor;
	void checkStr(string);
	void getClass(string, size_t);
	void parseSubItem(string);
	void dummyClass();
	vector<func_param> getParameters(string);
public:
	ClassProcessor();
	~ClassProcessor();
	bool processClass(string&, size_t);
};

#endif