/*c++11 please*/

#include <fstream> /*file access*/
#include <iostream> /* cin, cout*/
#include <cstdlib> /*exit(), */
#include <cstring> /* strlen(c_str)*/
#include <array>	/*	Duh  	*/
// #include <thread> /* multithreading */

#include "globals.h" /*<string> & <vector>*/
#include "folder.h"	/*parsing folders*/

using namespace std; 

// superglobals
string relpath = "./";
vector<char> opts; 				//short options: -o
vector<string> options;  		//long options: --option

int logLevel = 0;				
/*
-1 quiet: not even normal errors -q --quiet
0 normal
1 verbose -v --verbose
2 very verbose -vv
3 debug -vvv
.
.
.
*/

/*#
	writes to STDOUT according to verbosity level.
	#P str string to be written
	#P lvl logLevel needed if to be written (how many 'v's does it need to have this put out)
*/
void putOut(string str, int lvl)
{
	if (lvl <= logLevel)
		cout << str <<endl;
}


/*#
	parses all flags to set options accordingly.
*/
void checkFlags()
{
	putOut("checkFlags",3);
	unsigned int i;
	for (i = 0; i < opts.size(); ++i)
	{
		switch (opts[i])
		{
			case'v':
				if (logLevel == -1)
					logLevel++;
				logLevel++;
				break;
			
			case'q':	//no more quiet if verbose is set
				if (logLevel == 0)
					logLevel = -1;
				break;
		}
	}
	for (i = 0; i < options.size(); ++i)
	{
		if (options[i] == "quiet" && logLevel == 0)
		{
			logLevel = -1;
		}
		else if (options[i] == "verbose")
		{
			logLevel = 1;
		}
	}
}

/*#
	beginning of the program.
	will pass all the flags to checkFlags()
	makes the output directory
	and uses Folder.process() to start the program
	#R status code of program 0 is no errors occurred
*/
int main(int argc, char const *argv[])
{
	putOut("main",3);
	if (argc < 2)
	{
		cout << "Please input the src directory";
		cout << "\n\n\t"<< argv[0] <<" [path/to/src/]";
		cout << "\n\n\tfor help use: man ./pretty-man\n";
		return 1;
	}

string temp;
// check for terminal flags
	//check every argument
	for (int f = 1; f < argc; ++f)
	{
		//option: long or short
		if (argv[f][0] == '-')
		{
			//long option
			if (argv[f][1] == '-')
			{
				temp = "";
				unsigned int t;
				for (t = 2; t < strlen(argv[f]); ++t)
				{
					temp += argv[f][t];
				}
				options.push_back(temp);
			}

			//short option
			else
			{
				int i = 1;
				do{
					opts.push_back(argv[f][i]);
					i++;
				}
				while (argv[f][i] != 0);
			}

		}

		//no option: source dir
		else
			relpath = argv[f];
	}
	
	checkFlags();
	
	if (relpath == "./")
	{
		putOut("defaulting to current directory",1);
	}

	//add trailing slash if not existing
	else if (relpath[relpath.length()-1] != '/')
		relpath += '/';

	// putOut("error",-1);
	// putOut("normal",0);
	// putOut("verbose",1);
	// putOut("veryVerbose",2);
	// putOut("debug",3);
	// putOut("nonsense",4);

	Folder sourceFolder;
	//make outputfolder relative to relpath
	sourceFolder.makeFolder("../docs/");
	
	//call the folder class to start processing the folders
	if (! sourceFolder.listContent(relpath))
		putOut("Directory does not exist or is not readable",0);
	else
		if ( sourceFolder.processFiles())
			;





	//process all the data to a nice website
	//HTML htmlMaker();

	//yay!
	return 0;
}