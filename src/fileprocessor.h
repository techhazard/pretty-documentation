#ifndef __File_H_INCLUDED__
#define __File_H_INCLUDED__
/** #F
contains all the methods to parse a single sourcefile, header.
*/

#include "jsonmaker.h"
#include "file_instance.h"
#include "class_instance.h"


class FileProcessor
{
	int indentation; //level of {'s
	string location;
	std::fstream inFile; 
	file_instance fileComponent;
	JsonMaker writer;
	class_instance classInstance;
	string className;


	void checkStr(string);
	void getClass(string, size_t);
	void parseSubItem(string);
	void dummyClass();
	vector<func_param> getParameters(string);
	void checkClass(string);
	void parseFile();
	void checkForBraces	(string);
	string getOpeningBrace();
	bool processHeader();
	void dummyClass(class_instance&);
	string stripComment(string);
	bool openIfstream();
	void dummyContents();
public:
	bool processFile(string);
	FileProcessor();
	~FileProcessor();
	string takeLine();
	bool processClass(string&, size_t);
};






#endif