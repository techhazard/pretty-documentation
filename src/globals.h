#ifndef __GLOBALS_H_INCLUDED__
#define __GLOBALS_H_INCLUDED__

#include <vector>
#include <string>

using std::string; /*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/

extern string relpath;
extern std::vector<string> phpKeyWords;
extern std::vector<string> cppKeyWords;
extern std::vector<string> javaKeyWords;
extern std::vector<string> jsKeyWords;
extern std::vector<string> cKeyWords;
extern std::vector<string> pythonKeyWords;


extern std::vector<string> srcFiles;

//output according to verbosity level
extern void putOut(string, int);
#endif