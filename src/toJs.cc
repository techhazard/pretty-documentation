/*#F 
	This contains the program that puts writeouts in fron of html to make it "dynamic javascript"
	(my knowledge of JS doesn't go very far...)
*/

#include <fstream>
#include <string>
#include <iostream>
using namespace std;



/*#
escapes the given char in the string (prepends a \)

#P str the sting to process
#P ch1 the char to be escaped

#R the processed string
*/
string escapeChar(string str, char ch1) {

	string esc = "\\";

	for (int i = 0; i < str.length(); ++i) {
		if (str[i] == ch1 && i > 0)
		{
			str.insert(i,esc);
			i += 1;
		}
	}
	return str;
}



/*#
turns thhe given html file into a javascript file.

*/
int main(int argc, char const *argv[])
{
	if (argc < 2)
	{
		cout << "give name of html"<<endl;
		return 1;
	}
	fstream i;
	fstream o;
	for (int f = 1; f < argc; ++f)
	{
		string name = argv[f];
		string infile = "./" + name;
		name.erase(name.end() - 5,name.end());
		string outfile = "../" + name + ".js";
		i.open(infile.c_str(),ios::in);
		o.open(outfile.c_str(),ios::out);
		if (!(i.is_open()))
		{
			return 1;
		}
		o << "var w = document.writeln.bind(document);\n";
		char c[10000];
		string s;
		while (! i.eof())
		{
			i.getline(c,10000);
			s = escapeChar(c,'"');
			o << "w(\"" << s <<"\");\n";
		}
		i.close();
		o.close();
		cout << name<<".html parsed to ../"<<name<<".js"<<endl;
	}
return 0;
}