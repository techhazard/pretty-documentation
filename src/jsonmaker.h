#ifndef __JSON_H_INCLUDED__
#define __JSON_H_INCLUDED__
/*#F
ewffgfgffeqw
*/


#include <string>
#include <vector>
#include <array>

#include "file_instance.h" 
#include "class_instance.h" /*->std::vector|string */


class JsonMaker
{
private:
/*VARIABLES*/
	char mybuffer[4096];
	int level;
	string fileName;
	std::hash<string> hash_str;
	std::fstream outFile;

/*FUNCTIONS*/
	string makeHash(string);
	void putChar(char);										//for { and }
	void putString(string, string); 							//key-value
	void putStringVector(string, vector<string>&);					//key-value[]
	void putContentVector(string, vector<content>&);
	void putFunctionVector(string, vector<mem_function>&);
	void putVariableVector(string, vector<mem_variable>&);
	void putParameterVector(string, vector<func_param>&);
	void putInt(string, int);

	void indentLine();
	void indentLine(int);
	void removeLast();
	bool openOfstream(string);
	
public:
/*VARIABLES*/
/*FUNCTIONS*/
	JsonMaker();
	~JsonMaker();

	//external calls to write a Json file
	bool writeFile(file_instance);
	bool writeClass(class_instance);
};


#endif