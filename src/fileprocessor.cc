/**#F
contains all the methods to process a single sourcefile, source.
*/


#include <fstream>  /*file operations*/
#include <cstdlib>  /*exit()*/
#include "globals.h" /*<string> & <vector>, putout()*/
#include "jsonmaker.h"	/*json operations*/


#include "fileprocessor.h"

using namespace std;


/*#
	constructor for this class.
	inits all variables to default,
	opens ifstream (fstream(in)),
	calls processFile()
	and writes the details of the file to
	disk with an instance of the Json class.
	#R File instance
	#P path path to the file relative to program execution
*/
FileProcessor::FileProcessor()
{
	putOut("FileProcessor",3);
	indentation = 0;
	className = "";
	// delete writer;
	// writer = new JsonMaker;
}

FileProcessor::~FileProcessor()
{
	putOut("~FileProcessor",3);
	writer.writeFile(fileComponent);
	if (inFile.is_open())
		inFile.close();
}

bool FileProcessor::openIfstream()
{
	putOut("openIfstream",3);
	inFile.open(location.c_str(), fstream::in);

	if (!(inFile.is_open()))
	{
		putOut("Can't open file "+location, 0);
		return 0; //can't open stream
	}
	return 1; //opened stream
}

/*#
	Start processing the file.
	this takes every line in the file until EOF 
	and passes it to checkClass()
	#R nothing
*/
bool FileProcessor::processFile(string path)
{
	// fileComponent = new file_instance;
	putOut("processFile",3);
	location = path;
	fileComponent.name = location;
	if (!openIfstream())
		return 0;


	dummyContents();
	

	string str;
	while (! inFile.eof())
	{	
		str = takeLine();
		checkClass(str);
	}

	
	inFile.close();
	return 1; //successfully processed
}

/*#
	checks if current line contains 'class' keyword.
	this portion of the program will eventually need massive expansion
	#R nothing
	#P str string to check.
*/
void FileProcessor::checkClass(string str)
{
	putOut("checkClass",3);
	size_t pos = str.find("class ");
	if (pos != string::npos)
	{
		// classy = new ClassProcessor;
		processClass(str, pos);
	}
}


/*#
	Takes a indentation-free line without comments from the file.
	this takes a line from the member-ifstream, 
	strippes all the whitespace from the beginning
	and returns the string after is has been stripped of comments;

	this will guaranteed give a non-empty string* if eof is not reached.
	*striped of comments & beginning whitespace

	#R a new stripped line (empty if EOF)
	#see string FileProcessor::stripComment(string);
*/
string FileProcessor::takeLine()
{
	putOut("takeLine",3);
	string str = "";
	do 
	{
		getline(inFile,str);

		//space or tab or newline
		while ((str[0] == ' ' || str[0] == '\t' || str[0] == '\n')&& str != "")
		{
			//delete 1 char starting from 0 (delete first char)
			str = str.erase(0,1);
		}
		putOut(str,5);
		str = stripComment(str);
	}
	while (str == "" && !inFile.eof());
	return str;
}



/*#
	strips all c-style comments from a string.
	both //line and / * block * / will be stripped.
	#P str the string to be stripped
	#R the stripped string
*/
string FileProcessor::stripComment(string str)
{
	putOut("stripComment",3);
	size_t pos = str.find("//");
	size_t end;
	string temp;
	//line comment
	if (pos != string::npos)
	{
		return str.substr(0,pos);
	}
	else
	{	/*block comment*/     /* (can be more in one line) */
		do 
		{
			pos = str.find("/*");
			end = str.find("*/");
			temp = str.substr(0,pos);
			if (end != string::npos)
				temp += str.substr(end+2,string::npos);
			str = temp;
		}
		while(pos != string::npos);
		while ((temp[0] == ' ' || temp[0] == '\t' || temp[0] == '\n')&& temp != "")
		{
			//delete 1 char starting from 0 (delete first char)
			temp = temp.erase(0,1);
		}
		return temp;
	}
}




bool FileProcessor::processClass(string& line, size_t pos)
{
	putOut("processClass",3);
	// dummyClass();
	classInstance.name = "";
	size_t end = line.find(" ", pos+6) -1;
	if (end == string::npos)
		end = line.find("{", pos+6) -1;
	if (end == string::npos)
		end = line.find("\n", pos+6) -1;
	classInstance.name += line.substr(pos+6,end);

	checkStr(line);
	do {
		line = takeLine();
		checkStr(line);

	} while (indentation > 0);

	return 1;
}


/*#
	this parses a string that ends with a ';'.
	into a function or variable;
	#P str the string to parse;

*/
void FileProcessor::parseSubItem(string str)
{
	putOut("parseSubItem",3);
	putOut(str+"\n",4);
	size_t pos = str.find("(");
	size_t beg = str.rfind(" ");
	if (beg == string::npos)
		beg = 0;
	if (pos == string::npos)
	{
		putOut("mem_var",3);
		mem_variable variable;
		variable.name = str.substr(beg,str.length()-2);
		variable.type = str.substr(0,beg);
		classInstance.variable.push_back(variable);
	}
	else
	{
		mem_function function;
		//between '(' and first ' ' before it;
		function.name = str.substr(beg,pos);
		putOut("mem_func:\t"+function.name,3);
		func_param parameter;
		function.parameter = (getParameters(str));
	}
	
}



/*#
	parses parameters of a function.
	and returns them in a vector containing func_param instances.
	#R vector of parameters
	#P function a string containing the function
*/
vector<func_param> FileProcessor::getParameters(string function)
{
	putOut("getParameters",3);

	vector<func_param> parameters;
	func_param parameter;
	parameter.p = 0;
	parameter.type = "void";
	size_t beg = function.find("(");
	size_t mid;
	size_t end = function.rfind(" ",beg); //'reverse end' with rfind

	if (end != string::npos)
		parameter.type = function.substr(0,end -1);
	parameter.blurb = "dummy";
	//return value
	parameters.push_back(parameter);

	do
	{
		parameter.p++;
		end = function.find(",",beg);

		//no "," found
		if (end == string::npos)
		{
			//check for ")" last/only parameter
			end = function.find(")",beg);
			
			// ) is after last parameter or directly after (,
			//so no parameter to process
			if (end != string::npos)
				break;
		}
		//find separation between type and name
		mid = function.rfind(" ",end);
		if (mid == string::npos)
		{
			parameter.type = function.substr(beg,end);
			parameter.name = "";
		}
		else if (mid != string::npos)
		{
			parameter.type = function.substr(beg,mid);
			parameter.name = function.substr(mid,end);
		}
		// putOut("p_type:"+parameter.type +" p_name:"+parameter.name,3);
	}
	while(end != string::npos);

	return parameters;
}



/*#
	checks string for curly braces or ';'.
	will send to parseSubItem if ';' is found
	#P line the string to be checked
	#R void
*/
void FileProcessor::checkStr(string line)
{
	putOut("checkStr",3);
	for (unsigned int i = 0; i < line.length(); ++i)
		{
			switch (line[i])
			{
				case '{':
					indentation++;
					break;

				case '}':
					indentation--;
					break;

				case ';':
					parseSubItem(line.substr(0,i));
					break;
			}

		}
}

//temporary until class is read correctly
void FileProcessor::dummyClass()
{
	putOut("dummyClass",3);
	classInstance.blurb = "wqerwggf";
	classInstance.description = "wewggferqEWFDWQRWGFDG";

	mem_variable vari;
		vari.name = "var1";
		vari.id = "hash";
		vari.blurb = "fffdsfd";
		vari.description = "defeqwffbefwgfbefg";
		vari.type = "mem_variable";
		vari.hidden = 1;
	classInstance.variable.push_back(vari);
	putOut("push_var",-1);
		vari.name = "var2";
		vari.id = "hash";
		vari.blurb = "fffdsfd";
		vari.description = "defeqwffbefwgfbefg";
		vari.type = "int";
		vari.hidden = 1;
	classInstance.variable.push_back(vari);
	putOut("push_var",-1);

	mem_function func;
		func.name = "func1";
		func.blurb = "effdbg";
		func.description = "efgdgbfewgregewr";
		func_param params;
		putOut("mem_func",-1);
			params.p = 0;
			params.type = "void";
			params.name = "name";
			params.blurb = "nothing";
		func.parameter.push_back(params);
		putOut("push_parm",-1);
		func_param params2;
			params2.p = 1;
			params2.type = "int";
			params2.name = "parm";
			params2.blurb = "integerdfddfd";
		func.parameter.push_back(params2);
		putOut("push_parm",-1);

	classInstance.function.push_back(func);
	putOut("push_func",-1);
	classInstance.function.push_back(func);
	putOut("push_func",-1);
}

//temporary until file is read correctly
void FileProcessor::dummyContents()
{
	putOut("dummyContents",3);
	fileComponent.blurb = "wqerwggf";
	fileComponent.description = "wewggferqEWFDWQRWGFDG";
	fileComponent.included.push_back("<cstdlib>");
	fileComponent.included.push_back("file.h");
	content Content;
	Content.name = "FileClass";
	Content.id = "hash";
	Content.type = "class";
	fileComponent.contents.push_back(Content);
	Content.name = "FileClass2";
	fileComponent.contents.push_back(Content);
}
