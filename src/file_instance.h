#ifndef __FILE_INSTANCE_H_INCLUDED__
#define __FILE_INSTANCE_H_INCLUDED__

#include <string>
#include <vector>
using std::vector;
using std::string;

/*#
	classes and functions and the like
*/
struct content
{
	string name = "";
	string id = "";
	string type  = ""; //# class or file or func etc.
};

struct file_instance
{
	string name  = "";
	string id  = "";
	string blurb  = "";
	string description  = "";
	vector<string> included;
	vector<content> contents;

};
#endif